﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    BulletControl BC;
    [Header("Bullet Behaviour")]
    [Tooltip("Particle to be spawned when bullet hits an object.")]
    public GameObject bulletHitParticle;
    [Tooltip("Add a sound object")]
    public GameObject gunSound; //allows more control over audio and easily fixes problems -only ok on small projects
    public float bulletSpeed = 4f;
    public int damage;
    bool fired = false;
    float bulletLifeSpan = 4f;

    void Start()
    {
        BC = FindObjectOfType<BulletControl>();
        damage = BC.bulletDamage;
        fired = false;

        //checks if bullet has hit an object
        InvokeRepeating("DestroyBullet", bulletLifeSpan, bulletLifeSpan);

        //plays gun sound if available when bullet is spawned
        try { Instantiate(gunSound, this.transform.position, Quaternion.identity); }
        catch { Debug.Log("assign a shot sound obj to BulletBehavior"); }
    }
    void Update()
    {
        //bullet movement
        transform.Translate(Vector3.up * bulletSpeed * Time.deltaTime);
    }
    void OnCollisionEnter(Collision other)
    {
        Destroy(this.gameObject);
    }

    //destroys bullets after being alive for 5 seconds
    void DestroyBullet()
    {
        //this ensures that bullets don't build up in the inspector causing crashes
        if (fired)
        {
            Instantiate(bulletHitParticle, this.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
        if (!fired) { fired = true; }
    }
}
