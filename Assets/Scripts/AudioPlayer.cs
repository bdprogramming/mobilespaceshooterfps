﻿using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
        AudioSource aS;
        void Start() {
            aS = GetComponent<AudioSource>();
        }
        void PlayAudio() { 
            aS.Play(); 
        }
}
