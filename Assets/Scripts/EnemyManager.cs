﻿using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    //init
    [Header("ENEMY MANAGER")]
    public GameControl gc;

    [Tooltip("Drag and drop an enemy GameObject, or the prefab of one.")]
    public GameObject enemy;

    [Tooltip("Enemy will spawn wherever this object is located.")]
    public GameObject spawnPointObject;

    [Header("Enemy Settings")]
    [Tooltip("These will be auto-assigned if not filled in by user / dev.")]
    public float hitPoints = 100; //auto-assigned if no input is found
    public float enemySpeed = 1;  
    public int spawnDelay = 3;
    public int damage = 20;

    [HideInInspector]
    public float stopDistance; //this is just in case you want the enemy to stop and shoot, etc.

    int spawnPositionMaxLeft = -18;
    int spawnPositionMaxRight = 18;
    Vector3 randomizedSpawnLoc;
    Vector3 spawnPoint;

    //START
    private void Start()
    {
        //assigns enemy spawn position
        //Also just showing that I know try, catch, throw, finally
        try { spawnPoint = spawnPointObject.transform.position; }
        catch 
        {
            Debug.Log("No spawn point assigned to enemy manager. Value has been auto-filled.");
            spawnPoint = new Vector3(0, 3f, 25f); 
        }

        //repeatedly spawns enemies once started
        InvokeRepeating("SpawnEnemies", spawnDelay, spawnDelay);
    }

    //allows an enemy to be spawned repeatedly
    void SpawnEnemies()
    {
        //disallows enemies from spawning at start menu and on game over screen
        if (gc.state == GameState.InGame)
        { 
            //randomized x spawn location so enemies come from an line of spawnpoints rather than 1
            float xRandomized = UnityEngine.Random.Range(spawnPositionMaxLeft, spawnPositionMaxRight);
            randomizedSpawnLoc = new Vector3(xRandomized, spawnPoint.y, spawnPoint.z);

            //spawns new enemy 
            Instantiate(enemy, randomizedSpawnLoc, Quaternion.identity);
        }
    }
}