﻿using UnityEngine;
public class PlayerController : MonoBehaviour
{
    public PointsTracker pointsTracker;
    public int startingHitPoints = 100;
    public int hitPoints = 100;

    void Update()
    {
        //player camera / gyroscope control
        Camera.main.transform.localRotation = AndroidRotTracking.GyroRotation;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "ENEMY")
        { 
            hitPoints -= other.GetComponent<EnemyBehaviour>().damage;
            other.GetComponent<EnemyBehaviour>().Death();
            pointsTracker.points -= 1000;
        }
    }
}