﻿using UnityEngine;
using System.Collections;
    public enum GameState { AtStartMenu, InGame, GameOver }; //needs to be global 

public class GameControl : MonoBehaviour
{
    [HideInInspector] //ensures that no enum dropdown is shown in inspector
    public GameState state;

    public PointsTracker pointsTracker;
    public PlayerController player;
    public GameObject startScreen;
    public GameObject inGameGUI;
    public GameObject gameOverScreen;
    public GameObject[] enemies;

    void Start()
    {
        state = GameState.AtStartMenu;
    }
    void Update()
    {
        //ensures enemies are all destroyed at end of game
        if (state != GameState.InGame)
        {
            if(enemies != null)
            enemies = GameObject.FindGameObjectsWithTag("ENEMY");

            foreach (GameObject enemy in enemies)
            { 
                Destroy(enemy);
            }
        }

        //GAME STATES
        //checks state of game and stops enemies from spawning if game is not playing
        switch (state)
        {
            //START MENU 
            case GameState.AtStartMenu:

                startScreen.SetActive(true);
                inGameGUI.SetActive(false);
                gameOverScreen.SetActive(false);
                player.hitPoints = player.startingHitPoints;
                pointsTracker.ResetPoints();

                if (Input.GetButtonDown("Fire1"))
                {
                    state = GameState.InGame;
                }
                break;
            
            //IN GAME
            case GameState.InGame:

                startScreen.SetActive(false);
                inGameGUI.SetActive(true);
                gameOverScreen.SetActive(false);

                if (player.hitPoints <= 0)
                    state = GameState.GameOver;
                break;

            //GAME OVER
            case GameState.GameOver:

                inGameGUI.SetActive(false);
                gameOverScreen.SetActive(true);

                if (Input.GetButtonDown("Fire1"))
                    state = GameState.AtStartMenu;
                break;
        }
    }
}