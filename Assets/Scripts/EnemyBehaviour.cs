﻿using UnityEngine;
public class EnemyBehaviour : MonoBehaviour
{
    //init
    public Material originalMaterial;
    public GameObject deathParticle;
    public Material flashMaterial;
    public GameObject deathAudio;
    public MeshRenderer mR;
	public float stopDistance = 0;
	public float enemySpeed = .1f;
	public float hitPoints = 100;
    public float flashTime = .2f;
    public int pointsNumber;
    public int damage;
	EnemyManager eM;
	Transform player;          
    bool damaged;
    bool matNeedsUpdate = false;

    void Start()
	{
        //Only using FindObj because it allows us to not have a reference Enemy object in every scene
        player = FindObjectOfType<PlayerController>().GetComponent<Transform>(); 
		eM = FindObjectOfType<EnemyManager>();
        damage = FindObjectOfType<EnemyManager>().damage;
        originalMaterial = mR.material;
		stopDistance = eM.stopDistance;
		enemySpeed = eM.enemySpeed;
        hitPoints = eM.hitPoints;

        //checks if bullet hit - and changes materials for damage/flash
        InvokeRepeating("DetectDamage", flashTime, flashTime);
    }
	void Update()
	{
        //death 
        if(hitPoints <= 0)
            Death();

        //player detection and enemy movement
        Vector3 displacementFromTarget = (player.position - transform.position); //get player position and figures distance from this enemy to player
		Vector3 directionToTarget = displacementFromTarget.normalized; //gets the direction to the player
		Vector3 velocity = directionToTarget * enemySpeed; 
		Vector3 moveAmount = velocity * Time.deltaTime;

		// will return a straight line distance from object
		float magnitudeFromTarget = displacementFromTarget.magnitude; 

		//move to player
		if(magnitudeFromTarget > stopDistance)
			transform.Translate(moveAmount);
	}
    void OnCollisionEnter(Collision other)
    {
        //bullet hit
        if(other.transform.tag == "BULLET")
        {
            BulletBehaviour bullet = other.transform.GetComponent<BulletBehaviour>();
            hitPoints -= bullet.damage;
            Destroy(other.gameObject);
            damaged = true;
        }
    }
    void DetectDamage()
    {
        //the enemy flashes when shot and then returns to normal color
        if ((!damaged) && (matNeedsUpdate))
        {
            mR.material = originalMaterial;
            matNeedsUpdate = false;
        }
        else if (damaged)
        {
            mR.material = flashMaterial;
            matNeedsUpdate = true;
            damaged = false;
        }
        damaged = false;
    }

    //plays audio, puts off a particle (smoke/parts/blood/etc.) 
    //and destroys this enemy
    public void Death()
    {
        Instantiate(deathParticle, transform.position, Quaternion.identity);
        Instantiate(deathAudio, transform.position, Quaternion.identity);
        //again - only doing this for easy use of prefab instancing
        FindObjectOfType<PointsTracker>().points += 1000;
        Destroy(gameObject);
    }
}