﻿using System.Collections;
using UnityEngine;

public class BulletControl : MonoBehaviour
{
    public GameControl gameControl;
    public GameObject bulletSpawnPos;
    public GameObject bullet;
    public Transform bulletParent;

    //these need default assignment to avoid potential confusion. Do not remove
    public int bulletDamage = 20;
    public int bulletSpeed = 8;
    public float shotSpeed = .1f; 
    
    //bool shotfired = false;
    bool canShoot;

    void Start()
    {
        InvokeRepeating("HoldAndShoot", shotSpeed, shotSpeed);
        StartCoroutine(nextShotDelay(shotSpeed));
    }

    void Update()
    {
            if (gameControl.state == GameState.InGame)
            {
                if (Input.GetButtonDown("Fire1"))
                {
                        if (canShoot)
                        {
                            canShoot = false;
                            Shoot();
                        }
                }
            }
    }
    void Shoot()
    {
        Instantiate(bullet, 
                    bulletSpawnPos.transform.position, 
                    bulletSpawnPos.transform.rotation, bulletParent);
    }
    void HoldAndShoot()
    {
        if (!canShoot) 
            canShoot = true; 
        if (Input.GetButton("Fire1"))
            Shoot();
    }

    IEnumerator nextShotDelay(float shotDelay)
    {
        yield return new WaitForSeconds(shotDelay);
        canShoot = true;
    }
}
