﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterGivenTime : MonoBehaviour
{
    public float secondsToPlay = 2;

    bool particleHasPlayed = false;

    void Start()
    {
        InvokeRepeating(("Destroyer"), secondsToPlay, secondsToPlay);
    }
    void Destroyer()
    {
        if (particleHasPlayed)
        {
            if (particleHasPlayed) { Destroy(this.gameObject); }
        }
        particleHasPlayed = true;
    }
}