﻿using UnityEngine;
using TMPro;

public class PointsTracker : MonoBehaviour
{
    TMP_Text tmpText;

    public int points = 0;
    string stringPoints;

    const int zeroPoints = 0;

    private void Start()
    {
        tmpText = GetComponent<TMP_Text>();
        tmpText.text = stringPoints;
    }

    void Update()
    {
        stringPoints = points.ToString();
        tmpText.text = stringPoints;
    }

    //resets points to zero
    public void ResetPoints()
    { 
        points = zeroPoints;
    }
}
