This is a game built by Brad Donohoo in under 48 hours.
It took me somewhere between 25 and 30 hours to complete. 

Here are some of the game's features:

- A first person gyroscopic controller
- A custom gun and (somewhat) in-depth gun/bullet control system in which
  bullets/guns can be customized in a number of ways
- An enemy management controller that spawns infinite enemies
  within a given X range. The enemies and their behaviour can be customized in several ways as well. 
- A simple 3 state system that keeps track of what's going on in the game
  to prevent having to create and deal with multiple scenes
  The states are  STARTMENU > INGAME > GAMEOVER
- Enemies that flash when hit with bullets
- Enemies that find and fly to the player
- If the enemies hit the player, the player loses health and the enemy is destroyed.
  Health is updated in the GUI.
- When an enemy is destroyed they explodes with mesh-particles, play a sound and the player
  gains a customizable set of points
- A fully responsive GUI that fits several aspect ratios (should work great with any mobile device)
- A functioning responsive start menu 
- A game over screen / functionality that collects and destroys all enemies
  that are left in the scene, for a clean Game Over UI
- Points are kept on screen after game is finished to allow player to clearly see their score
- The gun makes a gunshot sound when it shoots, and the enemies make an explosion sound when they die
- A replay option at the end of the game that resets the players points and health
- Thoughtfully and carefully commented code that any programmer can easily read through
- A DestroyAfterGivenTime tool that ensures enemies, particles, and bullets are destroyed as
  soon as they have done their part, keeping the scene clean 
- Customized Headers, Tooltips, and sections have been made in some of the components for 
  ease of use between developers. 
- A wide variety of efficient coding techniques
- Custom particles

NOTES: 
This game was made while developing in 2560x1440 LANDSCAPE aspect ratio as that is my phone's resolution.
I cannot guarantee that the UI will scale properly to other phones, but it seems to do well in the GAME window
with all high resolution aspect ratios. Please test the game at 2560x1440 Landcape mode

Brad Donohoo -- 2-24-2022